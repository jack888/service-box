import zipfile
import os
import platform
import sys
import subprocess
import ctypes
import shutil
import stat
import getpass

class WindowsEnv:
    def __init__(self):
        pass

    def is_admin(self):
        try:
            return ctypes.windll.shell32.IsUserAnAdmin()
        except:
            return False

    def check_remove(self):
        if os.path.exists('service-box-windows-dependency'):
            os.system("rd/s/q  service-box-windows-dependency")
        return True
    
    def pull_dependency(self):
        if os.system("git clone https://gitee.com/dennis-kk/service-box-windows-dependency") != 0:
            print("Pull windows dependency failed")
            return False
        return True
   
    def set_protoc_env(self):
        cmd = r''' reg add "HKCU\Environment" /f /t REG_SZ /v Protoc_Home /d "C:\Program Files (x86)\protobuf" '''
        os.system(cmd)
        cmd = r''' reg add "HKCU\Environment" /f /t REG_SZ /v Path /d "%Protoc_Home%\lib;%Protoc_Home%\bin;%Path%;" '''
        os.system(cmd)

    def check_protoc_version(self):
        result = str(os.popen("protoc --version").read())
        if len(result) == 0:
            print("there is no protoc")
            return False
        tmp = result.strip('\n').replace('libprotoc ', '').split('.')
        min_version = int(tmp[0]) * 1000000 + int(tmp[1]) * 1000 + int(tmp[2])
        if min_version < 3010000:
            print(" Protoc vertion need greater than or equal 3010000, current version is %d " %min_version)
            return False
        else:
            return True

    def build(self):
        if platform.system() != "Windows":
            print("Only run on windows")
            return False
        if not self.is_admin():
            print("Need administrative priviledge")
            return False
        if os.system("java.exe -version") != 0:
            print("Please install JDK")
            return False
        if not self.check_protoc_version():
            return False
        
        if not (self.check_remove() and self.pull_dependency()):
            return False
        
        zip = zipfile.ZipFile('./service-box-windows-dependency/service-box-windows-dependency.zip', 'r')
        for file in zip.namelist(): 
            zip.extract(file, "./service-box-windows-dependency/")
        zip.close()
        if not os.path.exists("C:\\Program Files (x86)\\protobuf"):
            shutil.copytree("./service-box-windows-dependency/protobuf", "C:\\Program Files (x86)\\protobuf")
        return True

if __name__ == "__main__":
    env = WindowsEnv()
    if env.build() == False:
        sys.exit(1)
    else:
        sys.exit(0)
        