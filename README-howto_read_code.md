# 代码结构

+ servive-box<br>
    + [build](https://gitee.com/dennis-kk/service-box/tree/master/build) service box安装脚本
    + [publish](https://gitee.com/dennis-kk/service-box/tree/master/publish) service box发布脚本
        + [bin](https://gitee.com/dennis-kk/service-box/tree/master/publish/bin) 发布脚本、默认配置、web console页面文件
        + [config](https://gitee.com/dennis-kk/service-box/tree/master/publish/config) 发布配置
    + [src](https://gitee.com/dennis-kk/service-box/tree/master/src)
        + [argument](https://gitee.com/dennis-kk/service-box/tree/master/src/argument) service box启动参数接口
        + [box](https://gitee.com/dennis-kk/service-box/tree/master/src/box) service box核心启动类、核心类实现、接口
        + [command](https://gitee.com/dennis-kk/service-box/tree/master/src/command) 外部命令处理器接口
        + [config](https://gitee.com/dennis-kk/service-box/tree/master/src/config) service box配置接口
        + [console](https://gitee.com/dennis-kk/service-box/tree/master/src/console) service box web console接口
        + [csv](https://gitee.com/dennis-kk/service-box/tree/master/src/csv) CSV配置管理器接口
        + [detail](https://gitee.com/dennis-kk/service-box/tree/master/src/detail) service box接口实现类、内部类
            + [zookeeper](https://gitee.com/dennis-kk/service-box/tree/master/src/detail/zookeeper) 服务注册/发现zookeeper实现
        + [http](https://gitee.com/dennis-kk/service-box/tree/master/src/http) HTTP客户端、服务器接口
        + [lang](https://gitee.com/dennis-kk/service-box/tree/master/src/lang) 日志多国语言管理接口
        + [lib](https://gitee.com/dennis-kk/service-box/tree/master/src/lib) service box主函数入口
        + [redis](https://gitee.com/dennis-kk/service-box/tree/master/src/redis) Redis客户端接口
        + [repo](https://gitee.com/dennis-kk/service-box/tree/master/src/repo) 服务仓库
        + [scheduler](https://gitee.com/dennis-kk/service-box/tree/master/src/scheduler) 定时器管理器接口
        + [service_finder](https://gitee.com/dennis-kk/service-box/tree/master/src/service_finder) 服务发现接口
        + [service_register](https://gitee.com/dennis-kk/service-box/tree/master/src/service_register) 服务注册接口
        + [time](https://gitee.com/dennis-kk/service-box/tree/master/src/time) 时间系统接口
        + [util](https://gitee.com/dennis-kk/service-box/tree/master/src/util) 工具库
            + [lua](https://gitee.com/dennis-kk/service-box/tree/master/src/util/lua) lua工具库、lua脚本引擎、lua调试器
            + [websocket](https://gitee.com/dennis-kk/service-box/tree/master/src/util/websocket) WebSocket服务器
    + [thirdparty](https://gitee.com/dennis-kk/service-box/tree/master/thirdparty) 第三方库
    + [tools](https://gitee.com/dennis-kk/service-box/tree/master/tools) service box启动/关闭本机环境脚本，默认容器启动脚本
    + [unittest](https://gitee.com/dennis-kk/service-box/tree/master/unittest) 单元测试框架、测试用例
    + [win-proj](https://gitee.com/dennis-kk/service-box/tree/master/win-proj) Visual Studio community 2019解决方案、工程文件