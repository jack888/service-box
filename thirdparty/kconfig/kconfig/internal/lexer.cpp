/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "lexer.h"
#include "file.h"
#include "token.h"

Lexer::Lexer(const std::string &source, File *file) {
  _source = source;
  _inString = false;
  _file = file;
  _loc = 0;
  _row = 1;
  _column = 0;
  _current = 0;
}

Lexer::~Lexer() {
  _source = "";
  _file = 0;
  _current = 0;
  _loc = 0;
  _row = 1;
  _column = 0;

  std::list<Token *>::iterator iterator = _tokens.begin();

  for (; iterator != _tokens.end(); iterator++) {
    delete *iterator;
  }

  _tokens.clear();
}

Token *Lexer::current() { return _current; }

Token *Lexer::getNext() {
  if (_loc > _source.size()) {
    return 0;
  }

  bool done = false;
  Token *token = 0;

  while (!done && hasNext()) {
    int ch = _source.at(_loc);

    if (_inString && (ch != '"')) {
      token = doString();
      done = true;
    } else if (Token::isTerm(ch)) {
      token = doTerm(ch, _file->id(), _row, _column);
      done = true;
      next();
    } else if (ch == 'r') {
      if (need("require", 7)) {
        token = doRequire();
        done = true;
      } else {
        token = doLiteral();
        done = true;
      }
    } else if (ch == '\n') {
      newLine();
    } else if ((ch == '\r') || (ch == ' ') || (ch == '\t')) {
      next();
    } else if (ch == ';') {
      skipComment();
    } else if (ch == '"') {
      _inString = !_inString;
      token = doTerm('"', _file->id(), _row, _column);
      next();
      done = true;
    } else {
      token = doLiteral();
      done = true;
    }
  }

  return token;
}

Token *Lexer::doTerm(int ch, int /*fileId*/, int /*row*/, int /*column*/) {
  Token *token = new Token(Token::getTokenType(ch), _file->id(), _row, _column);
  _current = token;
  _tokens.push_back(token);
  return token;
}

Token *Lexer::doRequire() {
  Token *token = new Token(Token::REQUIRE, _file->id(), _row, _column);
  _current = token;
  _tokens.push_back(token);
  _loc += 7;
  _column += 7;
  return token;
}

void Lexer::skipComment() {
  while (hasNext()) {
    int ch = next();

    if (ch == '\n') {
      _row += 1;
      _column = 1;
      break;
    }
  }
}

Token *Lexer::doString() {
  std::string text;

  while (hasNext()) {
    char l = _source.at(_loc);

    if (l != '"') {
      text.push_back(l);
      next();
    } else {
      break;
    }
  }

  Token *token =
      new Token(Token::LITERAL, _file->id(), _row, _column, text.c_str());
  _current = token;
  _tokens.push_back(token);
  return token;
}

Token *Lexer::doLiteral() {
  std::string text;

  while (hasNext()) {
    char l = _source.at(_loc);

    if ((l != '"') && (l != ' ') && (l != '\t') && (l != '\n') && (l != '\r') &&
        !Token::isTerm(l)) {
      text.push_back(l);
      next();
    } else {
      break;
    }
  }

  Token *token =
      new Token(Token::LITERAL, _file->id(), _row, _column, text.c_str());
  _current = token;
  _tokens.push_back(token);
  return token;
}

void Lexer::newLine() {
  _row += 1;
  _column = 1;
  _loc += 1;
}

bool Lexer::hasNext() { return (_source.size() > _loc); }

int Lexer::next() {
  _column += 1;
  return _source.at(_loc++);
}

bool Lexer::need(const char *literal, int length) {
  if ((int)(_source.size() - _loc) < length) {
    return false;
  }

  for (int i = 0; i < length; i++) {
    if (literal[i] != _source.at(_loc + i)) {
      return false;
    }
  }

  return true;
}
