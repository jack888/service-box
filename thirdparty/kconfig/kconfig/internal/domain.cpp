/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <stdexcept>
#include <vector>

#include "../../../src/util/string_util.hh"
#include "domain.h"

Domain::Domain(const std::string &name, Domain *parent) {
  _parent = parent;
  _name = name;
  _cursor = _all.end();
  _fired = false;
}

Domain::~Domain() {
  std::map<std::string, Domain *>::iterator domainGuard = _domains.begin();

  for (; domainGuard != _domains.end(); domainGuard++) {
    delete domainGuard->second;
  }

  _domains.clear();
  std::map<std::string, Attribute *>::iterator attributeGuard =
      _attributes.begin();

  for (; attributeGuard != _attributes.end(); attributeGuard++) {
    delete attributeGuard->second;
  }

  _attributes.clear();
  _all.clear();
  _cursor = _all.end();
  _fired = false;
}

bool Domain::hasNext() {
  if (_all.empty()) {
    return false;
  }

  if (!_fired) {
    _cursor = _all.begin();
    _fired = true;
  } else {
    if (_cursor == _all.end()) {
      _cursor = _all.begin();
      return false;
    }
  }

  return true;
}

Attribute *Domain::next() {
  if (_cursor == _all.end()) {
    return 0;
  }

  Attribute *attribute = _cursor->second;
  ++_cursor;
  return attribute;
}

Domain *Domain::getParent() { return _parent; }

Domain *Domain::newChild(const std::string &name) {
  std::map<std::string, Domain *>::iterator iterator = _domains.find(name);

  if (iterator != _domains.end()) {
    throw std::runtime_error("duplicate domain");
  }

  Domain *child = new Domain(name, this);
  _domains.insert(std::make_pair(name, child));
  _all.insert(std::make_pair(name, child));
  return child;
}

bool Domain::has(const std::string &name) {
  std::vector<std::string> stmts;
  kratos::util::split(name, ".", stmts);
  std::vector<std::string>::iterator stmt = stmts.begin();
  if (stmt == stmts.end()) {
    return false;
  }

  Attribute *attribute = 0;

  for (; stmt != stmts.end(); stmt++) {
    if (0 != attribute) {
      if (attribute->isZone()) {
        Domain *domain = dynamic_cast<Domain *>(attribute);
        attribute = domain->find(*stmt);
      } else {
        stmt++;
        break;
      }
    } else {
      attribute = find(*stmt);
    }

    if (0 == attribute) {
      return false;
    }
  }

  return (stmt == stmts.end());
}

bool Domain::addAttribute(const std::string &name, Attribute *attribute) {
  std::map<std::string, Attribute *>::iterator iterator =
      _attributes.find(name);

  if (iterator != _attributes.end()) {
    throw std::runtime_error("duplicate attribute");
  }

  _attributes.insert(std::make_pair(name, attribute));
  _all.insert(std::make_pair(name, attribute));
  return true;
}

Attribute *Domain::get(const std::string &name) {
  std::vector<std::string> stmts;
  kratos::util::split(name, ".", stmts);
  std::vector<std::string>::iterator stmt = stmts.begin();
  if (stmt == stmts.end()) {
    return 0;
  }

  Attribute *attribute = 0;

  for (; stmt != stmts.end(); stmt++) {
    if (0 != attribute) {
      if (attribute->isZone()) {
        Domain *domain = dynamic_cast<Domain *>(attribute);
        attribute = domain->find(*stmt);
      } else {
        stmt++;
        break;
      }
    } else {
      attribute = find(*stmt);
    }

    if (0 == attribute) {
      return 0;
    }
  }

  if (stmt != stmts.end()) {
    return 0;
  }

  return attribute;
}

void Domain::trace(const std::string &tab) {
  std::map<std::string, Domain *>::iterator iterator = _domains.begin();

  for (; iterator != _domains.end(); iterator++) {
    std::cout << tab << _name << "=" << std::endl;
    iterator->second->trace(tab + "\t");
  }
}

Attribute *Domain::find(const std::string &name) {
  Attribute *attribute = findDomain(name);

  if (0 == attribute) {
    attribute = findAttribute(name);
  }

  return attribute;
}

Attribute *Domain::findDomain(const std::string &name) {
  std::map<std::string, Domain *>::iterator iterator = _domains.find(name);

  if (iterator == _domains.end()) {
    return 0;
  }

  return iterator->second;
}

Attribute *Domain::findAttribute(const std::string &name) {
  std::map<std::string, Attribute *>::iterator iterator =
      _attributes.find(name);

  if (iterator == _attributes.end()) {
    return 0;
  }

  return iterator->second;
}
