/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TOKEN_H
#define TOKEN_H

#include <string>

/**
 * @brief token
 */
class Token {
public:
    const static int EQUAL = 1;            // '='
    const static int DOMAIN_START = 2;     // '{'
    const static int DOMAIN_END = 3;       // '}'
    const static int ARRAY_START = 4;      // '('
    const static int ARRAY_END = 5;        // ')'
    const static int TABLE_START = 6;      // '<'
    const static int TABLE_END = 7;        // '>'
    const static int COMMA = 8;            // ','
    const static int QUOTE = 9;            // '"'
    const static int COLON = 10;           // ':'

    const static int LITERAL = 11;         // literal
    const static int REQUIRE = 12;         // 'require'

public:
    /**
     * @brief test whether is terminator
     */
    static bool isTerm(int ch);

    /**
     * @brief get token type via character
     */
    static int getTokenType(int ch);

public:
    /**
     * @brief ctor
     */
    Token(int type, int fileId, int row, int column, const char* literal = 0);

    /**
     * @brief dtor
     */
    ~Token();

    /**
     * @brief get token type
     */
    int type();

    /**
     * @brief get file ID
     */
    int getFileId();

    /**
     * @brief get row
     */
    int getRow();

    /**
     * @brief get column
     */
    int getColumn();

    /**
     * @brief get token text
     */
    const std::string& getText();

private:
    int         _type;    // token type
    int         _fileId;  // file ID
    int         _row;     // row
    int         _column;  // column
    std::string _text;    // text
};

#endif // TOKEN_H
