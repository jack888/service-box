#pragma once

#include <string>

#define VERSION_MAJOR 1 // major
#define VERSION_MINOR 0 // minor
#define VERSION_PATCH 12 // patch

/**
 * Returns version number string
 * 
 * \return version number string
 */
static inline std::string get_version_string() {
  return "version " +
      std::to_string(VERSION_MAJOR) +
      "." +
      std::to_string(VERSION_MINOR) +
      "." +
      std::to_string(VERSION_PATCH) + "-alpha";
}
