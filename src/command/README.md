# 命令处理器

提供处理外部命令的功能。

## 使用流程

```
// 获取命令处理器
command_ptr = getContext()->new_command();
```
```
// 设置
command_ptr->wait_for("yourcommand", 1000, [&](const std::string &req) -> std::string {
    return "your command result";
});
```