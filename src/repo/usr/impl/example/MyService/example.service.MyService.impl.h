#pragma once

#include "example.service.MyService.h"

class MyServiceImpl : public MyService {
public:
//implementation->
    MyServiceImpl();
    virtual ~MyServiceImpl();
    virtual bool onAfterFork(rpc::Rpc* rpc) override;
    virtual bool onBeforeDestory(rpc::Rpc* rpc) override;
    virtual void onTick(std::time_t ms) override;
    virtual std::int8_t method1(rpc::StubCallPtr call) override;
    virtual std::int16_t method2(rpc::StubCallPtr call) override;
    virtual std::int32_t method3(rpc::StubCallPtr call) override;
    virtual std::int64_t method4(rpc::StubCallPtr call) override;
    virtual std::uint8_t method5(rpc::StubCallPtr call) override;
    virtual std::uint16_t method6(rpc::StubCallPtr call) override;
    virtual std::uint32_t method7(rpc::StubCallPtr call) override;
    virtual std::uint64_t method8(rpc::StubCallPtr call) override;
    virtual bool method9(rpc::StubCallPtr call) override;
    virtual std::shared_ptr<std::string> method10(rpc::StubCallPtr call) override;
    virtual float method11(rpc::StubCallPtr call) override;
    virtual double method12(rpc::StubCallPtr call) override;
    virtual std::shared_ptr<Data> method13(rpc::StubCallPtr call) override;
    virtual std::shared_ptr<Dummy> method14(rpc::StubCallPtr call) override;
    virtual std::shared_ptr<std::vector<Data>> method15(rpc::StubCallPtr call) override;
    virtual std::shared_ptr<std::unordered_map<std::uint32_t,Data>> method16(rpc::StubCallPtr call) override;
    virtual std::shared_ptr<std::unordered_set<std::string>> method17(rpc::StubCallPtr call) override;
    virtual std::int32_t method18(rpc::StubCallPtr call, std::int8_t, std::int16_t, std::int32_t, std::int64_t, std::uint8_t, std::uint16_t, std::uint32_t, std::uint64_t, bool, const std::string&, float, double, const Data&, const std::vector<Data>&, const std::vector<std::string>&, const std::unordered_map<std::uint32_t,Data>&, const std::unordered_set<std::string>&) override;
//implementation<-
};

