#pragma once

#include "example.service.Login.h"

class LoginImpl : public Login {
public:
//implementation->
    LoginImpl();
    virtual ~LoginImpl();
    virtual bool onAfterFork(rpc::Rpc* rpc) override;
    virtual bool onBeforeDestory(rpc::Rpc* rpc) override;
    virtual void onTick(std::time_t ms) override;
    virtual std::uint64_t login(rpc::StubCallPtr call, const std::string&, const std::string&) override;
    virtual void logout(rpc::StubCallPtr call, std::uint64_t) override;
//implementation<-
};

