import os
import shutil

class Initializer:
    def check(self):
        return False
        
    def build_frontend(self):
        pass
        
    def build_backend(self):
        pass
        
    def init(self):
        return False
        
    def checkEnv(self):
        pass
        
class Builder:
    def __init__(self, options):
        self.options = options
       
    def check_idl_name(self, idl_name):
        if idl_name.endswith(".idl"):
            return idl_name
        else:
            return idl_name + ".idl"
         
    def deleteIdl(self, name):
        idl_name = self.check_idl_name(name)
        if not os.path.exists("src/idl/" + idl_name):
            print (name + " not found in repo.")
            return
        (base_name, _) = os.path.splitext(os.path.basename(idl_name))
        if os.path.exists("src/include/" + base_name):
            shutil.rmtree("src/include/" + base_name)
        if os.path.exists("src/proxy/" + base_name):
            shutil.rmtree("src/proxy/" + base_name)
        if os.path.exists("src/stub/" + base_name):
            shutil.rmtree("src/stub/" + base_name)
        if os.path.exists("src/idl/" + base_name + ".idl"):
            os.remove("src/idl/" + base_name + ".idl")
        if os.path.exists("src/" + base_name):
            shutil.rmtree("src/" + base_name)
        if os.path.exists("lib/" + base_name):
            shutil.rmtree("lib/" + base_name)
        if os.path.exists("lib/proxy/" + base_name):
            shutil.rmtree("lib/proxy/" + base_name)
        if os.path.exists("lib/stub/" + base_name):
            shutil.rmtree("lib/stub/" + base_name)
        if os.path.exists("tmp/" + base_name):
            shutil.rmtree("tmp/" + base_name)
            
    def updateIdl(self, name, service_name = None):
        idl_name = self.check_idl_name(name)
        if not os.path.exists("src/idl/" + idl_name):
            print (name + " not found in repo.")
            return
        if not os.path.exists("src/idl/"+idl_name):
            return
        else:
            self.addIdl2Repo(idl_name, service_name, False)
        
    def build_idl(self, name, sname = None):
        pass
        
    def addIdl2Repo(self, file_name, sname = None, add=True):
        pass
        
    def updateRoot(self):
        pass