#!/usr/bin/python

import sys
import platform

py_version = sys.version_info.major

class Options:
    def __init__(self):
        self.options = {}
        self.default_cpp_options()
        
    def default_cpp_options(self):
        self.options["cpp"] = {}
        self.options["cpp"]["machine"] = "-m64"
        self.options["cpp"]["debug"] = "true"
        if platform.system() == "Windows":
            self.options["cpp"]["protobuf_path"] = "C:/Program Files (x86)/protobuf"
        
    def add_option(self, type, name, opt):
        opt = opt.replace("\\", "\\\\")
        if self.options is None:
            self.options = {}

        if py_version == 3 :
            if not type in self.options: 
                self.options[type] = {}
        else:
            if not self.options.has_key(type):
                self.options[type] = {}

        self.options[type][name] = opt
        
    def get_option(self, type, name):
        if py_version == 3:
            if type in self.options:
                if name in self.options[type]:
                    return self.options[type][name]
        else:
            if self.options.has_key(type):
                if self.options[type].has_key(name):
                    return self.options[type][name]
        return None