# 安装依赖

## Windows

需要首先安装JDK8.

下载
```
https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
```
配置环境路径
```
https://www.cnblogs.com/liuhongfeng/p/4177568.html
```

安装cmake版本>3.5

下载
```
https://cmake.org/download/
```

安装Visual Studio Community 2019

下载
```
https://visualstudio.microsoft.com/zh-hans/vs/
```

1. 搭建单机运行环境

使用管理员权限运行如下命令:

```
cd build
install_win.bat
```

2. 单机环境启动/关闭

启动

```
tools目录下运行: start_win_local_env.bat
```

关闭
```
tools目录下运行: stop_win_local_env.bat
```

3. 发布

[发布默认容器](https://gitee.com/dennis-kk/service-box/tree/master/publish)

4. 启动默认容器

启动

```
tools目录下运行: start_win_box.bat
```

## Linux

1. 搭建单机运行环境

使用管理员权限运行如下命令:

```
cd build
chmod +x install_linux.sh
./install_linux.sh
```

2. 单机环境启动/关闭

启动

```
tools目录下运行: ./start_linux_local_env.sh
```

关闭
```
tools目录下运行: ./stop_linux_local_env.sh
```

3. 发布

[发布默认容器](https://gitee.com/dennis-kk/service-box/tree/master/publish)

4. 启动默认容器

启动

```
tools目录下运行: ./start_linux_box.sh
```

## Docker

```
docker pull denniswang1980/service_box:v1.0.12.alpha
docker run --name=service_box --restart=always -p6889:6889 -p10888:10888 -d denniswang1980/service_box:v1.0.12.alpha /usr/sbin/init
```
使用镜像启动容器, 需要开放6889, 10888端口，进入容器后:
```
docker exec -it service_box bash
```

1.克隆仓库:
```
git clone https://gitee.com/dennis-kk/service-box
```

2. 初始化仓库, ```service-box/build/```目录内运行：
```
chmod +x ./docker_build.sh
./docker_build.sh
```
3. 发布

[发布默认容器](https://gitee.com/dennis-kk/service-box/tree/master/publish)

4. 启动默认容器

启动

```
tools目录下运行: ./start_linux_box.sh
```
